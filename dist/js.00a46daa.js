// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  return newRequire;
})({"js/canvas/utils/geometry.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 *
 * @param {object} mouse
 * @param {number} mouse.x
 * @param {number} mouse.y
 * @param {object} figure
 * @param {number} figure.x
 * @param {number} figure.y
 * @param {number} figure.width
 * @param {number} figure.height
 * @returns {boolean}
 */
function isMouseOverFigure(mouse, figure) {
  return mouse.x > figure.x && mouse.x < figure.x + figure.width && mouse.y > figure.y && mouse.y < figure.y + figure.height;
}
/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {{top: number, left: number, bottom: number, right: number}}
 */


function getDistances(figureMain, figureSecondary) {
  return {
    top: distanceTop(figureMain, figureSecondary),
    bottom: distanceBottom(figureMain, figureSecondary),
    left: distanceLeft(figureMain, figureSecondary),
    right: distanceRight(figureMain, figureSecondary)
  };
}
/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {{}}
 */


function getNearestDirection(figureMain, figureSecondary) {
  var distances = getDistances(figureMain, figureSecondary);
  var nearestValue = Object.values(distances).reduce(function (result, distance) {
    return result < distance ? distance : result;
  }, 0);
  var nearestKey = Object.keys(distances).find(function (direction) {
    return distances[direction] === nearestValue;
  });
  return _defineProperty({}, nearestKey, nearestValue);
}
/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {number}
 */


function distanceLeft(figureMain, figureSecondary) {
  var distance = figureMain.x + figureMain.width - figureSecondary.x;
  return distance < 0 ? Math.abs(distance) : 0;
}
/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {number}
 */


function distanceRight(figureMain, figureSecondary) {
  var distance = figureSecondary.x + figureSecondary.width - figureMain.x;
  return distance < 0 ? Math.abs(distance) : 0;
}
/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {number}
 */


function distanceTop(figureMain, figureSecondary) {
  var distance = figureMain.y + figureMain.height - figureSecondary.y;
  return distance < 0 ? Math.abs(distance) : 0;
}
/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {number}
 */


function distanceBottom(figureMain, figureSecondary) {
  var distance = figureSecondary.y + figureSecondary.height - figureMain.y;
  return distance < 0 ? Math.abs(distance) : 0;
}
/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {boolean}
 */


function isCollisionXLeft(figureMain, figureSecondary) {
  return figureMain.x + figureMain.width >= figureSecondary.x;
}
/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {boolean}
 */


function isCollisionXRight(figureMain, figureSecondary) {
  return figureSecondary.x + figureSecondary.width >= figureMain.x;
}
/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {boolean}
 */


function isCollisionX(figureMain, figureSecondary) {
  return isCollisionXLeft(figureMain, figureSecondary) && isCollisionXRight(figureMain, figureSecondary);
}
/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {boolean}
 */


function isCollusionYTop(figureMain, figureSecondary) {
  return figureMain.y + figureMain.height >= figureSecondary.y;
}
/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {boolean}
 */


function isCollusionYBottom(figureMain, figureSecondary) {
  return figureSecondary.y + figureSecondary.height >= figureMain.y;
}
/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {boolean}
 */


function isCollisionY(figureMain, figureSecondary) {
  return isCollusionYTop(figureMain, figureSecondary) && isCollusionYBottom(figureMain, figureSecondary);
}
/**
 *
 * @param {object} figureMain
 * @param {number} figureMain.x
 * @param {number} figureMain.y
 * @param {number} figureMain.width
 * @param {number} figureMain.height
 * @param {object} figureSecondary
 * @param {number} figureSecondary.x
 * @param {number} figureSecondary.y
 * @param {number} figureSecondary.width
 * @param {number} figureSecondary.height
 * @returns {boolean}
 */


function isCollision(figureMain, figureSecondary) {
  return isCollisionX(figureMain, figureSecondary) && isCollisionY(figureMain, figureSecondary);
}

var _default = {
  distanceLeft: distanceLeft,
  distanceRight: distanceRight,
  distanceTop: distanceTop,
  distanceBottom: distanceBottom,
  getDistances: getDistances,
  getNearestDirection: getNearestDirection,
  isMouseOverFigure: isMouseOverFigure,
  isCollisionXLeft: isCollisionXLeft,
  isCollisionXRight: isCollisionXRight,
  isCollisionX: isCollisionX,
  isCollusionYTop: isCollusionYTop,
  isCollusionYBottom: isCollusionYBottom,
  isCollisionY: isCollisionY,
  isCollision: isCollision
};
exports.default = _default;
},{}],"js/canvas/Coupling.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _geometry = _interopRequireDefault(require("./utils/geometry"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Coupling =
/*#__PURE__*/
function () {
  /**
   *
   * @param {object} options
   * @param {number} options.offset
   */
  function Coupling(options) {
    _classCallCheck(this, Coupling);

    this.axiosX = false;
    this.axiosY = false;
    this.figures = [];
    this.startMouseX = null;
    this.startMouseY = null;
    this.isActive = false;
    this.offset = options.offset || 20;
  }

  _createClass(Coupling, [{
    key: "setAxios",

    /**
     *
     * @param {object} config
     * @param {number} config.x
     * @param {number} config.y
     */
    value: function setAxios(config) {
      var _config$x = config.x,
          x = _config$x === void 0 ? this.axiosX : _config$x,
          _config$y = config.y,
          y = _config$y === void 0 ? this.axiosY : _config$y;
      this.axiosX = x;
      this.axiosY = y;
    }
  }, {
    key: "setFigure",
    value: function setFigure(index, figure) {
      this.figures[index] = figure;
    }
    /**
     *
     * @param {object} mouse
     * @param {number} mouse.x
     * @param {number} mouse.y
     */

  }, {
    key: "setMouse",
    value: function setMouse(mouse) {
      var _mouse$x = mouse.x,
          x = _mouse$x === void 0 ? this.startMouseX : _mouse$x,
          _mouse$y = mouse.y,
          y = _mouse$y === void 0 ? this.startMouseY : _mouse$y;
      this.startMouseX = x;
      this.startMouseY = y;
    }
  }, {
    key: "reset",
    value: function reset() {
      this.startMouseX = null;
      this.startMouseY = null;
      this.isActive = false;
      this.axiosX = false;
      this.axiosY = false;
    }
    /**
     *
     * @param {object} draggingFigure
     * @param {object} mouse
     * @param {number} mouse.x
     * @param {number} mouse.y
     */

  }, {
    key: "update",
    value: function update(draggingFigure, mouse) {
      this.getJumpCorrection(mouse).forEach(function (correction) {
        if (!correction) return;
        draggingFigure.move({
          x: correction.x,
          y: correction.y
        });
      });
    }
    /**
     *
     * @param {object} draggingFigure
     * @param {object} figure
     * @returns {boolean}
     */

  }, {
    key: "isCoupling",
    value: function isCoupling(draggingFigure, figure) {
      return _geometry.default.isCollision(draggingFigure, this.getField(figure));
    }
    /**
     *
     * @param {object} mouse
     * @param {number} mouse.x
     * @param {number} mouse.y
     * @returns {boolean}
     */

  }, {
    key: "isOutOfField",
    value: function isOutOfField(mouse) {
      return this.getDistance(mouse) > this.offset;
    }
    /**
     *
     * @param {object} figure
     * @param {number} figure.x
     * @param {number} figure.y
     * @param {number} figure.width
     * @param {number} figure.height
     * @returns {{x: number, width: number, y: number, height: number}}
     */

  }, {
    key: "getField",
    value: function getField(figure) {
      return {
        x: figure.x - this.offset,
        y: figure.y - this.offset,
        width: figure.width + this.offset * 2,
        height: figure.height + this.offset * 2
      };
    }
    /**
     *
     * @param {object} mouse
     * @param {number} mouse.x
     * @param {number} mouse.y
     * @returns {{x: number, y: number}[]}
     */

  }, {
    key: "getJumpCorrection",
    value: function getJumpCorrection(mouse) {
      var _this = this;

      return this.figures.map(function (figure) {
        if (!figure) return;
        var left = figure.left ? figure.left : 0;
        var right = figure.right ? -figure.right : 0;
        var top = figure.top ? figure.top : 0;
        var bottom = figure.bottom ? -figure.bottom : 0;
        var x = left + right;
        var y = top + bottom;

        if (!_this.isActive) {
          _this.setMouse({
            x: mouse.x,
            y: mouse.y
          });
        }

        _this.isActive = true;

        _this.setAxios({
          x: _this.axiosX = Math.abs(x) > Math.abs(y),
          y: _this.axiosY = Math.abs(x) < Math.abs(y)
        });

        return {
          x: x,
          y: y
        };
      });
    }
    /**
     *
     * @param {object} distance
     * @param {number} distance.x
     * @param {number} distance.y
     * @returns {{x: number, y: number}}
     */

  }, {
    key: "getFigureCorrection",
    value: function getFigureCorrection(distance) {
      return {
        x: this.axiosX ? 0 : distance.x,
        y: this.axiosY ? 0 : distance.y
      };
    }
    /**
     *
     * @param {object} mouse
     * @param {number} mouse.x
     * @param {number} mouse.y
     * @returns {{x: number, y: number}}
     */

  }, {
    key: "getMouseCorrection",
    value: function getMouseCorrection(mouse) {
      return {
        x: this.axiosX ? this.startMouseX : mouse.x,
        y: this.axiosY ? this.startMouseY : mouse.y
      };
    }
    /**
     *
     * @param {object} mouse
     * @param {number} mouse.x
     * @param {number} mouse.y
     * @returns {number}
     */

  }, {
    key: "getDistance",
    value: function getDistance(mouse) {
      var distanceX = mouse.x - this.startMouseX;
      var distanceY = mouse.y - this.startMouseY;
      return Math.max(Math.abs(distanceX), Math.abs(distanceY));
    }
  }]);

  return Coupling;
}();

exports.default = Coupling;
},{"./utils/geometry":"js/canvas/utils/geometry.js"}],"js/canvas/Collision.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _geometry = _interopRequireDefault(require("./utils/geometry"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Collision =
/*#__PURE__*/
function () {
  function Collision() {
    _classCallCheck(this, Collision);

    this.isActive = false;
  }

  _createClass(Collision, [{
    key: "isCollision",
    value: function isCollision(draggingFigure, figure) {
      return _geometry.default.isCollision(draggingFigure, this.getField(figure));
    }
  }, {
    key: "isSomeIsFaced",
    value: function isSomeIsFaced(figures) {
      return figures.some(function (figure) {
        if (figure.isDragging) return false;
        return figure.isFaced;
      });
    }
    /**
     *
     * @param {object} figure
     * @param {number} figure.x
     * @param {number} figure.y
     * @param {number} figure.width
     * @param {number} figure.height
     * @returns {{x: number, width: number, y: number, height: number}}
     */

  }, {
    key: "getField",
    value: function getField(figure) {
      return {
        x: figure.x + 1,
        y: figure.y + 1,
        width: figure.width - 2,
        height: figure.height - 2
      };
    }
  }, {
    key: "update",
    value: function update(draggingFigure, figures) {
      this.isActive = this.isSomeIsFaced(figures);
      draggingFigure.setCollision(this.isActive);
    }
  }]);

  return Collision;
}();

exports.default = Collision;
},{"./utils/geometry":"js/canvas/utils/geometry.js"}],"js/canvas/Dragging.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _geometry = _interopRequireDefault(require("./utils/geometry"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Dragging =
/*#__PURE__*/
function () {
  function Dragging() {
    _classCallCheck(this, Dragging);

    this.isActive = false;
    this.index = null;
  }

  _createClass(Dragging, [{
    key: "getFigure",
    value: function getFigure(figures) {
      return figures[this.index];
    }
  }, {
    key: "setFigure",
    value: function setFigure(index) {
      this.isActive = true;
      this.index = index;
    }
    /**
     *
     * @param {object[]} figures
     * @param {string} figures[].color
     * @param {boolean} figures[].isDragging
     * @param {boolean} figures[].isFaced
     * @param {number} figures[].x
     * @param {number} figures[].y
     */

  }, {
    key: "setFiguresAfterDrag",
    value: function setFiguresAfterDrag(figures) {
      this.resetState();
      figures.forEach(function (figure) {
        figure.isDragging ? figure.set({
          color: figure.startColor,
          isDragging: false,
          isFaced: false,
          startX: figure.x,
          startY: figure.y
        }) : figure.set({
          color: figure.startColor,
          isFaced: false
        });
      });
    }
    /**
     *
     * @param {object[]} figures
     * @param {string} figures[].color
     * @param {boolean} figures[].isDragging
     * @param {boolean} figures[].isFaced
     * @param {number} figures[].x
     * @param {number} figures[].y
     */

  }, {
    key: "setFiguresBeforeDrag",
    value: function setFiguresBeforeDrag(figures) {
      this.resetState();
      figures.forEach(function (figure) {
        figure.isDragging ? figure.set({
          color: figure.startColor,
          isDragging: false,
          isFaced: false,
          x: figure.startX,
          y: figure.startY
        }) : figure.set({
          color: figure.startColor,
          isFaced: false
        });
      });
    }
  }, {
    key: "resetState",
    value: function resetState() {
      this.isActive = false;
      this.index = null;
    }
  }, {
    key: "reset",
    value: function reset(figures) {
      this.resetState(); // figures.forEach(figure => {
      //   figure.set({ isDragging: false });
      // });
    }
    /**
     *
     * @param {object[]} figures
     * @param {boolean} figures[].isDragging
     * @param {object} mouse
     * @param {number} mouse.x
     * @param {number} mouse.y
     */

  }, {
    key: "update",
    value: function update(figures, mouse) {
      var _this = this;

      figures.forEach(function (figure, index) {
        if (!_geometry.default.isMouseOverFigure(mouse, figure)) return;

        _this.setFigure(index);

        figure.set({
          isDragging: true
        });
      });
    }
  }]);

  return Dragging;
}();

exports.default = Dragging;
},{"./utils/geometry":"js/canvas/utils/geometry.js"}],"js/canvas/figures/Figure.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Figure =
/*#__PURE__*/
function () {
  /**
   *
   * @param {object} ctx - canvas context object
   */
  function Figure(ctx) {
    _classCallCheck(this, Figure);

    this.ctx = ctx;
  }

  _createClass(Figure, [{
    key: "draw",
    value: function draw() {
      throw Error("Declare abstract method 'draw' of 'Figure' class!");
    }
  }, {
    key: "move",
    value: function move() {
      throw Error("Declare abstract method 'move' of 'Figure' class!");
    }
  }, {
    key: "setDefault",
    value: function setDefault() {
      throw Error("Declare abstract method 'setDefault' of 'Figure' class!");
    }
  }, {
    key: "setCollision",
    value: function setCollision() {
      throw Error("Declare abstract method 'setCollision' of 'Figure' class!");
    }
  }, {
    key: "set",
    value: function set() {
      throw Error("Declare abstract method 'set' of 'Figure' class!");
    }
  }]);

  return Figure;
}();

exports.default = Figure;
},{}],"js/canvas/figures/RectFigure.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Figure2 = _interopRequireDefault(require("./Figure"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var RectFigure =
/*#__PURE__*/
function (_Figure) {
  _inherits(RectFigure, _Figure);

  /**
   *
   * @param {object} ctx - canvas context object
   * @param {object} config
   * @param {number} config.x
   * @param {number} config.y
   * @param {number} config.width
   * @param {number} config.height
   * @param {string} config.color
   * @param {string} config.collisionColor
   */
  function RectFigure(ctx, config) {
    var _this;

    _classCallCheck(this, RectFigure);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(RectFigure).call(this, ctx));
    _this.defaultX = 0;
    _this.defaultY = 0;
    _this.defaultWidth = 50;
    _this.defaultHeight = 50;
    _this.defaultColor = "green";
    _this.defaultCollisionColor = "red";

    _this.setDefault(config);

    return _this;
  }

  _createClass(RectFigure, [{
    key: "draw",
    value: function draw() {
      this.ctx.fillStyle = this.color;
      this.ctx.fillRect(this.x, this.y, this.width, this.height);
    }
    /**
     *
     * @param {object} distance
     * @param {number} distance.x
     * @param {number} distance.y
     */

  }, {
    key: "move",
    value: function move(distance) {
      this.x = this.x + distance.x;
      this.y = this.y + distance.y;
    }
  }, {
    key: "setDefault",
    value: function setDefault(config) {
      var _config$x = config.x,
          x = _config$x === void 0 ? this.defaultX : _config$x,
          _config$y = config.y,
          y = _config$y === void 0 ? this.defaultY : _config$y,
          _config$width = config.width,
          width = _config$width === void 0 ? this.defaultWidth : _config$width,
          _config$height = config.height,
          height = _config$height === void 0 ? this.defaultHeight : _config$height,
          _config$color = config.color,
          color = _config$color === void 0 ? this.defaultColor : _config$color,
          _config$collisionColo = config.collisionColor,
          collisionColor = _config$collisionColo === void 0 ? this.defaultCollisionColor : _config$collisionColo;
      this.x = x;
      this.y = y;
      this.width = width;
      this.height = height;
      this.color = color;
      this.collisionColor = collisionColor;
      this.isDragging = false;
      this.isFaced = false;
      this.startColor = color;
      this.startX = x;
      this.startY = y;
    }
    /**
     *
     * @param {boolean} value
     */

  }, {
    key: "setCollision",
    value: function setCollision(value) {
      this.color = value ? this.collisionColor : this.startColor;
      this.isFaced = value;
    }
  }, {
    key: "set",
    value: function set(config) {
      var _config$x2 = config.x,
          x = _config$x2 === void 0 ? this.x : _config$x2,
          _config$y2 = config.y,
          y = _config$y2 === void 0 ? this.y : _config$y2,
          _config$width2 = config.width,
          width = _config$width2 === void 0 ? this.width : _config$width2,
          _config$height2 = config.height,
          height = _config$height2 === void 0 ? this.height : _config$height2,
          _config$color2 = config.color,
          color = _config$color2 === void 0 ? this.color : _config$color2,
          _config$collisionColo2 = config.collisionColor,
          collisionColor = _config$collisionColo2 === void 0 ? this.collisionColor : _config$collisionColo2,
          _config$isDragging = config.isDragging,
          isDragging = _config$isDragging === void 0 ? this.isDragging : _config$isDragging,
          _config$isFaced = config.isFaced,
          isFaced = _config$isFaced === void 0 ? this.isFaced : _config$isFaced,
          _config$startColor = config.startColor,
          startColor = _config$startColor === void 0 ? this.startColor : _config$startColor,
          _config$startX = config.startX,
          startX = _config$startX === void 0 ? this.startX : _config$startX,
          _config$startY = config.startY,
          startY = _config$startY === void 0 ? this.startY : _config$startY;
      this.x = x;
      this.y = y;
      this.width = width;
      this.height = height;
      this.color = color;
      this.collisionColor = collisionColor;
      this.isDragging = isDragging;
      this.isFaced = isFaced;
      this.startColor = startColor;
      this.startX = startX;
      this.startY = startY;
    }
  }]);

  return RectFigure;
}(_Figure2.default);

exports.default = RectFigure;
},{"./Figure":"js/canvas/figures/Figure.js"}],"js/canvas/Canvas.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Coupling = _interopRequireDefault(require("./Coupling"));

var _Collision = _interopRequireDefault(require("./Collision"));

var _Dragging = _interopRequireDefault(require("./Dragging"));

var _RectFigure = _interopRequireDefault(require("./figures/RectFigure"));

var _geometry = _interopRequireDefault(require("./utils/geometry"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Canvas =
/*#__PURE__*/
function () {
  /**
   *
   * @param {object} el - canvas DOM element
   * @param {object[]} figures - list configs of figures
   * @param {number} figures[].width
   * @param {number} figures[].height
   * @param {string} figures[].color
   * @param {object} options
   * @param {number} options.basePositionOffset
   * @param {number} options.couplingOffset
   */
  function Canvas(el, figures, options) {
    var _this = this;

    _classCallCheck(this, Canvas);

    this.startMouseX = null;
    this.startMouseY = null;

    this.handleMousedownEvent = function (e) {
      e.preventDefault();
      e.stopPropagation();
      var mouse = {
        x: e.layerX,
        y: e.layerY
      };

      _this.dragging.update(_this.figures, mouse);

      _this.updateStartMouse(mouse);
    };

    this.handleMousemoveEvent = function (e) {
      if (!_this.dragging.isActive) return;
      e.preventDefault();
      e.stopPropagation();
      var mouse = {
        x: e.layerX,
        y: e.layerY
      };
      var distance = {
        x: mouse.x - _this.startMouseX,
        y: mouse.y - _this.startMouseY
      };
      if (_this.coupling.isActive) return _this.updateCoupling(distance, mouse);

      _this.dragging.getFigure(_this.figures).move(distance);

      _this.updateAndDraw();

      _this.updateStartMouse(mouse);
    };

    this.handleMouseupEvent = function (e) {
      e.preventDefault();
      e.stopPropagation();
      _this.collision.isActive ? _this.dragging.setFiguresBeforeDrag(_this.figures) : _this.dragging.setFiguresAfterDrag(_this.figures);

      _this.draw();
    };

    this.element = el;
    this.ctx = el.getContext("2d");
    this.basePositionOffset = options.basePositionOffset || 30;
    this.dragging = new _Dragging.default();
    this.coupling = new _Coupling.default({
      offset: options.couplingOffset
    });
    this.collision = new _Collision.default();
    this.figures = this.getDefaultFigures(figures);
    this.addMouseEventListeners();
  }

  _createClass(Canvas, [{
    key: "addMouseEventListeners",
    // EVENTS
    value: function addMouseEventListeners() {
      this.element.addEventListener("mousedown", this.handleMousedownEvent);
      this.element.addEventListener("mouseup", this.handleMouseupEvent);
      this.element.addEventListener("mousemove", this.handleMousemoveEvent);
    }
  }, {
    key: "getDefaultFigures",
    // COMPUTED
    value: function getDefaultFigures(figures) {
      var _this2 = this;

      var prevHeightSum = 0;
      return figures.map(function (figure) {
        var y = prevHeightSum + _this2.basePositionOffset;
        prevHeightSum = prevHeightSum + _this2.basePositionOffset + figure.height;
        return new _RectFigure.default(_this2.ctx, _objectSpread({}, figure, {
          x: _this2.basePositionOffset,
          y: y
        }));
      });
    } // INNER

  }, {
    key: "draw",
    value: function draw() {
      this.updateSize();
      this.figures.forEach(function (figure) {
        figure.draw();
      });
    }
    /**
     *
     * @param {object} distance
     * @param {number} distance.x
     * @param {number} distance.y
     */

  }, {
    key: "moveDraggingFigureWithCoupling",
    value: function moveDraggingFigureWithCoupling(distance) {
      this.dragging.getFigure(this.figures).move(this.coupling.getFigureCorrection(distance));
    }
  }, {
    key: "updateAndDraw",
    value: function updateAndDraw() {
      this.updateNotDraggingFigures();
      this.updateCollision();
      this.updateDraggingFigure();
      this.draw();
    }
  }, {
    key: "updateCollision",
    value: function updateCollision() {
      this.collision.update(this.dragging.getFigure(this.figures), this.figures);
    }
  }, {
    key: "updateCoupling",
    value: function updateCoupling(distance, mouse) {
      if (this.coupling.isOutOfField(mouse)) return this.coupling.reset();
      this.moveDraggingFigureWithCoupling(distance);
      this.updateAndDraw();
      this.updateStartMouseWithCoupling(mouse);
    }
  }, {
    key: "updateDraggingFigure",
    value: function updateDraggingFigure() {
      if (this.collision.isActive) return;
      var draggingFigure = this.dragging.getFigure(this.figures);
      this.coupling.update(draggingFigure, {
        x: this.startMouseX,
        y: this.startMouseY
      });
    }
  }, {
    key: "updateNotDraggingFigures",
    value: function updateNotDraggingFigures() {
      var _this3 = this;

      this.figures.forEach(function (figure, index) {
        if (figure.isDragging) return;

        var draggingFigure = _this3.dragging.getFigure(_this3.figures);

        var isCollision = _this3.collision.isCollision(draggingFigure, figure);

        var isCoupling = !isCollision && _this3.coupling.isCoupling(draggingFigure, figure);

        figure.setCollision(isCollision);

        _this3.coupling.setFigure(index, isCoupling ? _geometry.default.getNearestDirection(draggingFigure, figure) : null);
      });
    }
    /**
     *
     * @param {object} mouse
     * @param {number} mouse.x
     * @param {number} mouse.y
     */

  }, {
    key: "updateStartMouse",
    value: function updateStartMouse(mouse) {
      this.startMouseX = mouse.x;
      this.startMouseY = mouse.y;
    }
    /**
     *
     * @param {object} mouse
     * @param {number} mouse.x
     * @param {number} mouse.y
     */

  }, {
    key: "updateStartMouseWithCoupling",
    value: function updateStartMouseWithCoupling(mouse) {
      this.updateStartMouse(this.coupling.getMouseCorrection(mouse));
    }
  }, {
    key: "updateSize",
    value: function updateSize() {
      var rect = this.element.getBoundingClientRect();
      var horizontalOffset = rect.left + (rect.right - rect.width);
      var verticalOffset = rect.top + (rect.bottom - rect.height);
      this.element.width = window.innerWidth - horizontalOffset;
      this.element.height = window.innerHeight - verticalOffset;
    }
  }]);

  return Canvas;
}();

exports.default = Canvas;
},{"./Coupling":"js/canvas/Coupling.js","./Collision":"js/canvas/Collision.js","./Dragging":"js/canvas/Dragging.js","./figures/RectFigure":"js/canvas/figures/RectFigure.js","./utils/geometry":"js/canvas/utils/geometry.js"}],"js/App.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Canvas = _interopRequireDefault(require("./canvas/Canvas"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var App =
/*#__PURE__*/
function () {
  /**
   *
   * @param {string} id - id of canvas DOM element
   * @param {object[]} figures - list configs of figures
   * @param {number} figures[].width
   * @param {number} figures[].height
   * @param {string} figures[].color
   * @param {object} options
   * @param {number} options.basePositionOffset
   * @param {number} options.couplingOffset
   * @param {string} options.collisionColor
   */
  function App(id, figures, options) {
    var _this = this;

    _classCallCheck(this, App);

    this.handleLoad = function () {
      _this.canvas = new _Canvas.default(document.getElementById(_this.elementId), _this.figures, _this.options || {});

      _this.canvas.draw();
    };

    this.handleResize = function () {
      _this.canvas.draw();
    };

    if (_typeof(App.instance) === "object") return App.instance;
    this.elementId = id;
    this.figures = figures;
    this.options = options || {};
    this.addDocumentEventListeners();
    App.instance = this;
    return this;
  } // EVENTS


  _createClass(App, [{
    key: "addDocumentEventListeners",
    value: function addDocumentEventListeners() {
      window.addEventListener("load", this.handleLoad);
      window.addEventListener("resize", this.handleResize, false);
    }
  }]);

  return App;
}();

exports.default = App;
},{"./canvas/Canvas":"js/canvas/Canvas.js"}],"js/index.js":[function(require,module,exports) {
"use strict";

var _App = _interopRequireDefault(require("./App"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var canvasElId = "canvas";
var listOfFigures = [{
  width: 100,
  height: 100,
  color: "green"
}, {
  width: 100,
  height: 50,
  color: "blue"
}, {
  width: 200,
  height: 200,
  color: "yellow"
}, {
  width: 50,
  height: 75,
  color: "purple"
}];
new _App.default(canvasElId, listOfFigures);
},{"./App":"js/App.js"}],"../../.npm-global/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "34241" + '/');

  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      console.clear();
      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id);
  });
}
},{}]},{},["../../.npm-global/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","js/index.js"], null)
//# sourceMappingURL=/js.00a46daa.map