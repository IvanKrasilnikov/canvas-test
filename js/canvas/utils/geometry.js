/**
 *
 * @param {object} mouse
 * @param {number} mouse.x
 * @param {number} mouse.y
 * @param {object} figure
 * @param {number} figure.x
 * @param {number} figure.y
 * @param {number} figure.width
 * @param {number} figure.height
 * @returns {boolean}
 */
function isMouseOverFigure(mouse, figure) {
  return (
    mouse.x > figure.x &&
    mouse.x < figure.x + figure.width &&
    mouse.y > figure.y &&
    mouse.y < figure.y + figure.height
  );
}

/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {{top: number, left: number, bottom: number, right: number}}
 */
function getDistances(figureMain, figureSecondary) {
  return {
    top: distanceTop(figureMain, figureSecondary),
    bottom: distanceBottom(figureMain, figureSecondary),
    left: distanceLeft(figureMain, figureSecondary),
    right: distanceRight(figureMain, figureSecondary)
  };
}

/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {{}}
 */
function getNearestDirection(figureMain, figureSecondary) {
  const distances = getDistances(figureMain, figureSecondary);
  const nearestValue = Object.values(distances).reduce(
    (result, distance) => (result < distance ? distance : result),
    0
  );
  const nearestKey = Object.keys(distances).find(
    direction => distances[direction] === nearestValue
  );

  return {
    [nearestKey]: nearestValue
  };
}

/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {number}
 */
function distanceLeft(figureMain, figureSecondary) {
  const distance = figureMain.x + figureMain.width - figureSecondary.x;

  return distance < 0 ? Math.abs(distance) : 0;
}

/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {number}
 */
function distanceRight(figureMain, figureSecondary) {
  const distance = figureSecondary.x + figureSecondary.width - figureMain.x;

  return distance < 0 ? Math.abs(distance) : 0;
}

/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {number}
 */
function distanceTop(figureMain, figureSecondary) {
  const distance = figureMain.y + figureMain.height - figureSecondary.y;

  return distance < 0 ? Math.abs(distance) : 0;
}

/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {number}
 */
function distanceBottom(figureMain, figureSecondary) {
  const distance = figureSecondary.y + figureSecondary.height - figureMain.y;

  return distance < 0 ? Math.abs(distance) : 0;
}

/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {boolean}
 */
function isCollisionXLeft(figureMain, figureSecondary) {
  return figureMain.x + figureMain.width >= figureSecondary.x;
}

/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {boolean}
 */
function isCollisionXRight(figureMain, figureSecondary) {
  return figureSecondary.x + figureSecondary.width >= figureMain.x;
}

/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {boolean}
 */
function isCollisionX(figureMain, figureSecondary) {
  return (
    isCollisionXLeft(figureMain, figureSecondary) &&
    isCollisionXRight(figureMain, figureSecondary)
  );
}

/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {boolean}
 */
function isCollusionYTop(figureMain, figureSecondary) {
  return figureMain.y + figureMain.height >= figureSecondary.y;
}

/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {boolean}
 */
function isCollusionYBottom(figureMain, figureSecondary) {
  return figureSecondary.y + figureSecondary.height >= figureMain.y;
}

/**
 *
 * @param figureMain
 * @param figureSecondary
 * @returns {boolean}
 */
function isCollisionY(figureMain, figureSecondary) {
  return (
    isCollusionYTop(figureMain, figureSecondary) &&
    isCollusionYBottom(figureMain, figureSecondary)
  );
}

/**
 *
 * @param {object} figureMain
 * @param {number} figureMain.x
 * @param {number} figureMain.y
 * @param {number} figureMain.width
 * @param {number} figureMain.height
 * @param {object} figureSecondary
 * @param {number} figureSecondary.x
 * @param {number} figureSecondary.y
 * @param {number} figureSecondary.width
 * @param {number} figureSecondary.height
 * @returns {boolean}
 */
function isCollision(figureMain, figureSecondary) {
  return (
    isCollisionX(figureMain, figureSecondary) &&
    isCollisionY(figureMain, figureSecondary)
  );
}

export default {
  distanceLeft,
  distanceRight,
  distanceTop,
  distanceBottom,
  getDistances,
  getNearestDirection,
  isMouseOverFigure,
  isCollisionXLeft,
  isCollisionXRight,
  isCollisionX,
  isCollusionYTop,
  isCollusionYBottom,
  isCollisionY,
  isCollision
};
