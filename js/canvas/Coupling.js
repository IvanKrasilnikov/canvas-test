import geometry from "./utils/geometry";

export default class Coupling {
  /**
   *
   * @param {object} options
   * @param {number} options.offset
   */
  constructor(options) {
    this.offset = options.offset || 20;
  }

  axiosX = false;
  axiosY = false;
  figures = [];
  startMouseX = null;
  startMouseY = null;
  isActive = false;

  /**
   *
   * @param {object} config
   * @param {number} config.x
   * @param {number} config.y
   */
  setAxios(config) {
    const { x = this.axiosX, y = this.axiosY } = config;

    this.axiosX = x;
    this.axiosY = y;
  }

  setFigure(index, figure) {
    this.figures[index] = figure;
  }

  /**
   *
   * @param {object} mouse
   * @param {number} mouse.x
   * @param {number} mouse.y
   */
  setMouse(mouse) {
    const { x = this.startMouseX, y = this.startMouseY } = mouse;

    this.startMouseX = x;
    this.startMouseY = y;
  }

  reset() {
    this.startMouseX = null;
    this.startMouseY = null;
    this.isActive = false;
    this.axiosX = false;
    this.axiosY = false;
  }

  /**
   *
   * @param {object} draggingFigure
   * @param {object} mouse
   * @param {number} mouse.x
   * @param {number} mouse.y
   */
  update(draggingFigure, mouse) {
    this.getJumpCorrection(mouse).forEach(correction => {
      if (!correction) return;

      draggingFigure.move({ x: correction.x, y: correction.y });
    });
  }

  /**
   *
   * @param {object} draggingFigure
   * @param {object} figure
   * @returns {boolean}
   */
  isCoupling(draggingFigure, figure) {
    return geometry.isCollision(draggingFigure, this.getField(figure));
  }

  /**
   *
   * @param {object} mouse
   * @param {number} mouse.x
   * @param {number} mouse.y
   * @returns {boolean}
   */
  isOutOfField(mouse) {
    return this.getDistance(mouse) > this.offset;
  }

  /**
   *
   * @param {object} figure
   * @param {number} figure.x
   * @param {number} figure.y
   * @param {number} figure.width
   * @param {number} figure.height
   * @returns {{x: number, width: number, y: number, height: number}}
   */
  getField(figure) {
    return {
      x: figure.x - this.offset,
      y: figure.y - this.offset,
      width: figure.width + this.offset * 2,
      height: figure.height + this.offset * 2
    };
  }

  /**
   *
   * @param {object} mouse
   * @param {number} mouse.x
   * @param {number} mouse.y
   * @returns {{x: number, y: number}[]}
   */
  getJumpCorrection(mouse) {
    return this.figures.map(figure => {
      if (!figure) return;

      const left = figure.left ? figure.left : 0;
      const right = figure.right ? -figure.right : 0;
      const top = figure.top ? figure.top : 0;
      const bottom = figure.bottom ? -figure.bottom : 0;
      const x = left + right;
      const y = top + bottom;

      if (!this.isActive) {
        this.setMouse({
          x: mouse.x,
          y: mouse.y
        });
      }

      this.isActive = true;
      this.setAxios({
        x: (this.axiosX = Math.abs(x) > Math.abs(y)),
        y: (this.axiosY = Math.abs(x) < Math.abs(y))
      });

      return { x, y };
    });
  }

  /**
   *
   * @param {object} distance
   * @param {number} distance.x
   * @param {number} distance.y
   * @returns {{x: number, y: number}}
   */
  getFigureCorrection(distance) {
    return {
      x: this.axiosX ? 0 : distance.x,
      y: this.axiosY ? 0 : distance.y
    };
  }

  /**
   *
   * @param {object} mouse
   * @param {number} mouse.x
   * @param {number} mouse.y
   * @returns {{x: number, y: number}}
   */
  getMouseCorrection(mouse) {
    return {
      x: this.axiosX ? this.startMouseX : mouse.x,
      y: this.axiosY ? this.startMouseY : mouse.y
    };
  }

  /**
   *
   * @param {object} mouse
   * @param {number} mouse.x
   * @param {number} mouse.y
   * @returns {number}
   */
  getDistance(mouse) {
    const distanceX = mouse.x - this.startMouseX;
    const distanceY = mouse.y - this.startMouseY;
    return Math.max(Math.abs(distanceX), Math.abs(distanceY));
  }
}
