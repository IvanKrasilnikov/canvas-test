import Figure from "./Figure";

export default class RectFigure extends Figure {
  /**
   *
   * @param {object} ctx - canvas context object
   * @param {object} config
   * @param {number} config.x
   * @param {number} config.y
   * @param {number} config.width
   * @param {number} config.height
   * @param {string} config.color
   * @param {string} config.collisionColor
   */
  constructor(ctx, config) {
    super(ctx);

    this.setDefault(config);
  }

  defaultX = 0;
  defaultY = 0;
  defaultWidth = 50;
  defaultHeight = 50;
  defaultColor = "green";
  defaultCollisionColor = "red";

  draw() {
    this.ctx.fillStyle = this.color;
    this.ctx.fillRect(this.x, this.y, this.width, this.height);
  }

  /**
   *
   * @param {object} distance
   * @param {number} distance.x
   * @param {number} distance.y
   */
  move(distance) {
    this.x = this.x + distance.x;
    this.y = this.y + distance.y;
  }

  setDefault(config) {
    const {
      x = this.defaultX,
      y = this.defaultY,
      width = this.defaultWidth,
      height = this.defaultHeight,
      color = this.defaultColor,
      collisionColor = this.defaultCollisionColor
    } = config;

    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.color = color;
    this.collisionColor = collisionColor;
    this.isDragging = false;
    this.isFaced = false;
    this.startColor = color;
    this.startX = x;
    this.startY = y;
  }

  /**
   *
   * @param {boolean} value
   */
  setCollision(value) {
    this.color = value ? this.collisionColor : this.startColor;
    this.isFaced = value;
  }

  set(config) {
    const {
      x = this.x,
      y = this.y,
      width = this.width,
      height = this.height,
      color = this.color,
      collisionColor = this.collisionColor,
      isDragging = this.isDragging,
      isFaced = this.isFaced,
      startColor = this.startColor,
      startX = this.startX,
      startY = this.startY
    } = config;

    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.color = color;
    this.collisionColor = collisionColor;
    this.isDragging = isDragging;
    this.isFaced = isFaced;
    this.startColor = startColor;
    this.startX = startX;
    this.startY = startY;
  }
}
