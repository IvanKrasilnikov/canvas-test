export default class Figure {
  /**
   *
   * @param {object} ctx - canvas context object
   */
  constructor(ctx) {
    this.ctx = ctx;
  }

  draw() {
    throw Error("Declare abstract method 'draw' of 'Figure' class!");
  }

  move() {
    throw Error("Declare abstract method 'move' of 'Figure' class!");
  }

  setDefault() {
    throw Error("Declare abstract method 'setDefault' of 'Figure' class!");
  }

  setCollision() {
    throw Error("Declare abstract method 'setCollision' of 'Figure' class!");
  }

  set() {
    throw Error("Declare abstract method 'set' of 'Figure' class!");
  }
}
