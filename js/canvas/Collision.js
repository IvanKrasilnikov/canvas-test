import geometry from "./utils/geometry";

export default class Collision {
  isActive = false;

  isCollision(draggingFigure, figure) {
    return geometry.isCollision(draggingFigure, this.getField(figure));
  }

  isSomeIsFaced(figures) {
    return figures.some(figure => {
      if (figure.isDragging) return false;

      return figure.isFaced;
    });
  }

  /**
   *
   * @param {object} figure
   * @param {number} figure.x
   * @param {number} figure.y
   * @param {number} figure.width
   * @param {number} figure.height
   * @returns {{x: number, width: number, y: number, height: number}}
   */
  getField(figure) {
    return {
      x: figure.x + 1,
      y: figure.y + 1,
      width: figure.width - 2,
      height: figure.height - 2
    };
  }

  update(draggingFigure, figures) {
    this.isActive = this.isSomeIsFaced(figures);
    draggingFigure.setCollision(this.isActive);
  }
}
