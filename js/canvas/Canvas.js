import Coupling from "./Coupling";
import Collision from "./Collision";
import Dragging from "./Dragging";
import RectFigure from "./figures/RectFigure";
import geometry from "./utils/geometry";

export default class Canvas {
  /**
   *
   * @param {object} el - canvas DOM element
   * @param {object[]} figures - list configs of figures
   * @param {number} figures[].width
   * @param {number} figures[].height
   * @param {string} figures[].color
   * @param {object} options
   * @param {number} options.basePositionOffset
   * @param {number} options.couplingOffset
   */
  constructor(el, figures, options) {
    this.element = el;
    this.ctx = el.getContext("2d");
    this.basePositionOffset = options.basePositionOffset || 30;
    this.dragging = new Dragging();
    this.coupling = new Coupling({
      offset: options.couplingOffset
    });
    this.collision = new Collision();
    this.figures = this.getDefaultFigures(figures);

    this.addMouseEventListeners();
  }

  startMouseX = null;
  startMouseY = null;

  // EVENTS

  addMouseEventListeners() {
    this.element.addEventListener("mousedown", this.handleMousedownEvent);
    this.element.addEventListener("mouseup", this.handleMouseupEvent);
    this.element.addEventListener("mousemove", this.handleMousemoveEvent);
  }

  handleMousedownEvent = e => {
    e.preventDefault();
    e.stopPropagation();

    const mouse = {
      x: e.layerX,
      y: e.layerY
    };

    this.dragging.update(this.figures, mouse);
    this.updateStartMouse(mouse);
  };

  handleMousemoveEvent = e => {
    if (!this.dragging.isActive) return;

    e.preventDefault();
    e.stopPropagation();

    const mouse = {
      x: e.layerX,
      y: e.layerY
    };

    const distance = {
      x: mouse.x - this.startMouseX,
      y: mouse.y - this.startMouseY
    };

    if (this.coupling.isActive) return this.updateCoupling(distance, mouse);

    this.dragging.getFigure(this.figures).move(distance);
    this.updateAndDraw();
    this.updateStartMouse(mouse);
  };

  handleMouseupEvent = e => {
    e.preventDefault();
    e.stopPropagation();

    this.collision.isActive
      ? this.dragging.setFiguresBeforeDrag(this.figures)
      : this.dragging.setFiguresAfterDrag(this.figures);

    this.draw();
  };

  // COMPUTED

  getDefaultFigures(figures) {
    let prevHeightSum = 0;

    return figures.map(figure => {
      const y = prevHeightSum + this.basePositionOffset;
      prevHeightSum = prevHeightSum + this.basePositionOffset + figure.height;

      return new RectFigure(this.ctx, {
        ...figure,
        x: this.basePositionOffset,
        y
      });
    });
  }

  // INNER

  draw() {
    this.updateSize();

    this.figures.forEach(figure => {
      figure.draw();
    });
  }

  /**
   *
   * @param {object} distance
   * @param {number} distance.x
   * @param {number} distance.y
   */
  moveDraggingFigureWithCoupling(distance) {
    this.dragging
      .getFigure(this.figures)
      .move(this.coupling.getFigureCorrection(distance));
  }

  updateAndDraw() {
    this.updateNotDraggingFigures();
    this.updateCollision();
    this.updateDraggingFigure();
    this.draw();
  }

  updateCollision() {
    this.collision.update(this.dragging.getFigure(this.figures), this.figures);
  }

  updateCoupling(distance, mouse) {
    if (this.coupling.isOutOfField(mouse)) return this.coupling.reset();

    this.moveDraggingFigureWithCoupling(distance);
    this.updateAndDraw();
    this.updateStartMouseWithCoupling(mouse);
  }

  updateDraggingFigure() {
    if (this.collision.isActive) return;

    const draggingFigure = this.dragging.getFigure(this.figures);
    this.coupling.update(draggingFigure, {
      x: this.startMouseX,
      y: this.startMouseY
    });
  }

  updateNotDraggingFigures() {
    this.figures.forEach((figure, index) => {
      if (figure.isDragging) return;

      const draggingFigure = this.dragging.getFigure(this.figures);
      const isCollision = this.collision.isCollision(draggingFigure, figure);
      const isCoupling =
        !isCollision && this.coupling.isCoupling(draggingFigure, figure);

      figure.setCollision(isCollision);

      this.coupling.setFigure(
        index,
        isCoupling ? geometry.getNearestDirection(draggingFigure, figure) : null
      );
    });
  }

  /**
   *
   * @param {object} mouse
   * @param {number} mouse.x
   * @param {number} mouse.y
   */
  updateStartMouse(mouse) {
    this.startMouseX = mouse.x;
    this.startMouseY = mouse.y;
  }

  /**
   *
   * @param {object} mouse
   * @param {number} mouse.x
   * @param {number} mouse.y
   */
  updateStartMouseWithCoupling(mouse) {
    this.updateStartMouse(this.coupling.getMouseCorrection(mouse));
  }

  updateSize() {
    const rect = this.element.getBoundingClientRect();
    const horizontalOffset = rect.left + (rect.right - rect.width);
    const verticalOffset = rect.top + (rect.bottom - rect.height);

    this.element.width = window.innerWidth - horizontalOffset;
    this.element.height = window.innerHeight - verticalOffset;
  }
}
