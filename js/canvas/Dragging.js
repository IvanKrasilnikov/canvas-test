import geometry from "./utils/geometry";

export default class Dragging {
  isActive = false;
  index = null;

  getFigure(figures) {
    return figures[this.index];
  }

  setFigure(index) {
    this.isActive = true;
    this.index = index;
  }

  /**
   *
   * @param {object[]} figures
   * @param {string} figures[].color
   * @param {boolean} figures[].isDragging
   * @param {boolean} figures[].isFaced
   * @param {number} figures[].x
   * @param {number} figures[].y
   */
  setFiguresAfterDrag(figures) {
    this.resetState();

    figures.forEach(figure => {
      figure.isDragging
        ? figure.set({
            color: figure.startColor,
            isDragging: false,
            isFaced: false,
            startX: figure.x,
            startY: figure.y
          })
        : figure.set({
            color: figure.startColor,
            isFaced: false
          });
    });
  }

  /**
   *
   * @param {object[]} figures
   * @param {string} figures[].color
   * @param {boolean} figures[].isDragging
   * @param {boolean} figures[].isFaced
   * @param {number} figures[].x
   * @param {number} figures[].y
   */
  setFiguresBeforeDrag(figures) {
    this.resetState();

    figures.forEach(figure => {
      figure.isDragging
        ? figure.set({
            color: figure.startColor,
            isDragging: false,
            isFaced: false,
            x: figure.startX,
            y: figure.startY
          })
        : figure.set({
            color: figure.startColor,
            isFaced: false
          });
    });
  }

  resetState() {
    this.isActive = false;
    this.index = null;
  }

  reset(figures) {
    this.resetState();

    // figures.forEach(figure => {
    //   figure.set({ isDragging: false });
    // });
  }

  /**
   *
   * @param {object[]} figures
   * @param {boolean} figures[].isDragging
   * @param {object} mouse
   * @param {number} mouse.x
   * @param {number} mouse.y
   */
  update(figures, mouse) {
    figures.forEach((figure, index) => {
      if (!geometry.isMouseOverFigure(mouse, figure)) return;

      this.setFigure(index);

      figure.set({ isDragging: true });
    });
  }
}
