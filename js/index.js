import App from "./App";

const canvasElId = "canvas";
const listOfFigures = [
  {
    width: 100,
    height: 100,
    color: "green"
  },
  {
    width: 100,
    height: 50,
    color: "blue"
  },
  {
    width: 200,
    height: 200,
    color: "yellow"
  },
  {
    width: 50,
    height: 75,
    color: "purple"
  }
];

new App(canvasElId, listOfFigures);
