import Canvas from "./canvas/Canvas";

export default class App {
  /**
   *
   * @param {string} id - id of canvas DOM element
   * @param {object[]} figures - list configs of figures
   * @param {number} figures[].width
   * @param {number} figures[].height
   * @param {string} figures[].color
   * @param {object} options
   * @param {number} options.basePositionOffset
   * @param {number} options.couplingOffset
   * @param {string} options.collisionColor
   */
  constructor(id, figures, options) {
    if (typeof App.instance === "object") return App.instance;

    this.elementId = id;
    this.figures = figures;
    this.options = options || {};

    this.addDocumentEventListeners();

    App.instance = this;
    return this;
  }

  // EVENTS

  addDocumentEventListeners() {
    window.addEventListener("load", this.handleLoad);
    window.addEventListener("resize", this.handleResize, false);
  }

  handleLoad = () => {
    this.canvas = new Canvas(
      document.getElementById(this.elementId),
      this.figures,
      this.options || {}
    );

    this.canvas.draw();
  };

  handleResize = () => {
    this.canvas.draw();
  };
}
